package com.example.dell.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dell.controllers.models.PersonaModel;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
@RequestMapping(value = "/consulta", consumes = { MediaType.APPLICATION_JSON_VALUE})
public class ConsultaUsuarios {

	@PostMapping("/persona" )
	@HystrixCommand()
	public String pagomovilBDV(@RequestBody(required=false) PersonaModel personaModel) {
		System.out.println("PERSONA");
		return "PERSONA";
	}
	
	@PostMapping("/empresa")
	@HystrixCommand()
	public String pagoMovilOtroBanco(@RequestBody(required=false) PersonaModel personaModel) {
		System.out.println("EMPRESA");
		return "EMPRESA";
	}
}
